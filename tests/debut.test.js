const puppeteer = require("puppeteer");


test("Проверяем наличие темы Голландская защита", async () => {
    const browser = await puppeteer.launch();
    try {
        const page = await browser.newPage();

        await page.goto("https://stepchess.ru/debut");


        let mateInOneText = await page.$("#wrp > div > div > div > div > a:nth-child(2) > div > div > div > div.mt-2");
        let value = await mateInOneText.evaluate((el) => el.textContent);

        expect(value).toContain("Голландская защита");
    } finally {
        await browser.close();
    }
}, 120000);

