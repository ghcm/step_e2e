const puppeteer = require("puppeteer");


describe('Проверяем табы на главной странице', () => {
    
    test("Проверяем наличие таба Тактика", async () => {
        const browser = await puppeteer.launch();
        try {
            const page = await browser.newPage();

            await page.goto("https://stepchess.ru");


            let tacticTab = await page.$("#wrp > div > div > div > div > ul > li:nth-child(1) > a");
            let value = await tacticTab.evaluate((el) => el.textContent);

            expect(value).toContain("Тактика");
        } finally {
            await browser.close();
        }
    }, 120000);


    test("Проверяем наличие таба Дебюты", async () => {
        const browser = await puppeteer.launch();
        try {
            const page = await browser.newPage();

            await page.goto("https://stepchess.ru");


            let debutTab = await page.$("#wrp > div > div > div > div > ul > li:nth-child(2) > a");
            let value = await debutTab.evaluate((el) => el.textContent);

            expect(value).toContain("Дебюты");
        } finally {
            await browser.close();
        }
    }, 120000);


test("Проверяем наличие таба Эндшпиль", async () => {
    const browser = await puppeteer.launch();
    try {
        const page = await browser.newPage();

        await page.goto("https://stepchess.ru");


        let endspilTab = await page.$("#wrp > div > div > div > div > ul > li:nth-child(3) > a");
        let value = await endspilTab.evaluate((el) => el.textContent);

        expect(value).toContain("Эндшпиль");
    } finally {
        await browser.close();
    }
}, 120000);


test("Проверяем наличие таба Для начинающих", async () => {
    const browser = await puppeteer.launch();
    try {
        const page = await browser.newPage();

        await page.goto("https://stepchess.ru");


        let beginnerTab = await page.$("#wrp > div > div > div > div > ul > li:nth-child(4) > a");
        let value = await beginnerTab.evaluate((el) => el.textContent);

        expect(value).toContain("Для начинающих");
    } finally {
        await browser.close();
    }
}, 120000);


test("Проверяем наличие таба Раздел тренера", async () => {
    const browser = await puppeteer.launch();
    try {
        const page = await browser.newPage();

        await page.goto("https://stepchess.ru");


        let trenerTab = await page.$("#wrp > div > div > div > div > ul > li:nth-child(5) > a");
        let value = await trenerTab.evaluate((el) => el.textContent);

        expect(value).toContain("Раздел тренера");
    } finally {
        await browser.close();
    }
}, 120000);

});


